# Script Name : password_check.py
# Author      : ASSOULINE Jordan
# Created     : November 28th 2022
# Version     : 1.5
# Description : Check if your password is too worst & use logging

import logging
logging.basicConfig(filename='password_check.log',
format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
level=logging.DEBUG)
logging.getLogger().name="main"
logging.info("Starting Script...")

user_password = input("Please enter the password to check: \n")

def worst_500_passwords(password):
  # Define the logger name
  logging.getLogger().name="worst_500_passwords"
  
  #Download the SecList
  import requests
  logging.info('Beginning check in 500 worst passwords')
  url = 'https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/500-worst-passwords.txt'
  logging.debug('Try to connect to the URL {}'.format(url))
  retrieve = requests.get(url)

  if retrieve.status_code == 404:
    logging.critical("Can't access the URL, check the connection or the URL")
    exit(2)
  elif retrieve.status_code == 200:
    logging.debug('500 worst passwords list correctly downloaded')
  else:
    logging.warning('Status_code different from 404 or 200 : {}'.format(retrieve.status_code))

  content = retrieve.content
  logging.info('Access to the list content')
  
  if str(password) in str(content):
    print("Your password is in 500 worst passwords... Change it immediately\n")
    logging.info('Password is in 500 worst passwords')
  else:
    logging.info('Password is not in 500 worst passwords')

def worst_10k_passwords(password):
  # Define the logger name
  logging.getLogger().name="worst_10k_passwords"
 
  #Download the SecList
  import requests
  logging.info('Beginning check in 10K worst passwords')
  url = 'https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/10k-most-common.txt'
  logging.debug('Try to connect to the URL {}'.format(url))
  retrieve = requests.get(url)
  
  if retrieve.status_code == 404:
    logging.critical("Can't access the URL, check the connection or the URL")
    exit(2)
  elif retrieve.status_code == 200:
    logging.debug('500 worst passwords list correctly downloaded')
  else:
    logging.warning('Status_code different from 404 or 200 : {}'.format(retrieve.status_code))

  content = retrieve.content
  logging.info('Access to the list content')

  if str(password) in str(content):
    print("Your password is in 10K worst passwords... Change it immediately\n")
    logging.info('Password is in 10k worst passwords')
  else:
    logging.info('Password is not in 10k worst passwords')

if __name__ == "__main__":
  worst_500_passwords(user_password)
  worst_10k_passwords(user_password)
  logging.getLogger().name="main"
  logging.info("Ending Script...")
